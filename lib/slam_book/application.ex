defmodule SlamBook.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      SlamBook.Repo,
      # Start the endpoint when the application starts
      SlamBookWeb.Endpoint
      # Starts a worker by calling: SlamBook.Worker.start_link(arg)
      # {SlamBook.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SlamBook.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SlamBookWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

defmodule SlamBook.Repo do
  use Ecto.Repo,
    otp_app: :slam_book,
    adapter: Ecto.Adapters.Postgres
end

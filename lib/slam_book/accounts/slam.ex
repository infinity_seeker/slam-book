defmodule SlamBook.Accounts.Slam do
  use Ecto.Schema
  import Ecto.Changeset

  schema "slams" do
    field :advice, :string
    field :biggest_negative, :string
    field :biggest_positive, :string
    field :biggest_regret, :string
    field :fancied_me, :string
    field :first_impression, :string
    field :friendship_agremeent, :string
    field :had_crush, :string
    field :last_impression, :string
    field :opinion, :string
    field :personlality, :string
    field :suggestion, :string
    field :the_reason, :string
    field :things_you_hate, :string
    field :things_you_like, :string
    belongs_to :user, SlamBook.Accounts.User
    field :filler_name, :string
    timestamps()
  end

  @doc false
  def changeset(slam, attrs) do
    slam
    |> cast(attrs, [:first_impression, :last_impression, :biggest_positive, :biggest_negative, :fancied_me, :the_reason, :things_you_like, :things_you_hate, :biggest_regret, :personlality, :opinion, :had_crush, :suggestion, :friendship_agremeent, :advice, :user_id, :filler_name])
    |> validate_required([:first_impression, :last_impression, :biggest_positive, :biggest_negative, :fancied_me, :things_you_like, :things_you_hate, :biggest_regret, :personlality, :suggestion, :friendship_agremeent, :advice, :user_id])
  end
end

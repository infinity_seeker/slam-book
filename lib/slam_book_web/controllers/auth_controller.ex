defmodule SlamBook.Auth do
  import Plug.Conn
  import Phoenix.Controller
  alias SlamBookWeb.Router.Helpers

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, _) do
    user_id = get_session(conn, :user_id)
    # IO.inspect(user_id)
    cond do
      user = conn.assigns[:current_user] ->
        conn
      user = user_id && SlamBook.Accounts.get_user!(user_id) ->
        put_current_user(conn, user)
      true ->
        put_current_user(conn, nil)
    end
  end

  def put_current_user(conn, user) do
    conn
    |> assign(:current_user, user)
    |> assign(:admin_user, !!user && user.credential && user.credential.email == "minatsilvester@gmail.com")
  end

  def login(conn, user) do
    conn
    |> put_current_user(user)
    |> put_session(:user_id, user.id)
    |> configure_session(renrew: true)
    |> redirect(to: Helpers.user_path(conn, :index))
  end

  def logout(conn) do
    conn
    |> configure_session(drop: true)
  end

  # def logged_in_user(conn, _) do
  #   user_id = get_session(conn, :user_id)
  #   if user_id != nil do
  #     conn
  #   else
  #     conn
  #     |> put_flash(:error, "Please Login")
  #     |> redirect(to: Helpers.session_path(conn, :new))
  #   end
  # end

  def logged_in_user(conn = %{assigns: %{current_user: %{}}}, _), do: conn

  def logged_in_user(conn, _) do
    IO.inspect(conn)
    conn
    |> put_flash(:error, "Please Login")
    |> redirect(to: Helpers.session_path(conn, :new))
  end

end

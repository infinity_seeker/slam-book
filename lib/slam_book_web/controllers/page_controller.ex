defmodule SlamBookWeb.PageController do
  use SlamBookWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end

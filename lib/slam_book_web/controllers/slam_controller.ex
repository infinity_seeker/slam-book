defmodule SlamBookWeb.SlamController do
  use SlamBookWeb, :controller

  alias SlamBook.Accounts
  alias SlamBook.Accounts.Slam
  plug :logged_in_user when action not in [:new, :create]
  plug :no_self_slam when action in [:new, :create]
  plug :editing when action in [:edit]
  # plug :no_delete when action in [:delete]
  # plug :view_my_slam when action in [:index]

  def action(conn, _) do
    user = Accounts.get_user!(conn.params["user_id"])
    args = [conn, conn.params, user]
    apply(__MODULE__, action_name(conn), args)
  end

  def index(conn, _params, user) do
    IO.inspect(conn)
    slams = SlamBook.Accounts.list_slams(user)
    render(conn, "index.html", slams: slams, user: user)
  end

  def new(conn = %{assigns: %{current_user: current_user}}, _params, user) do
    slams = Accounts.list_slams(user)
    IO.inspect(current_user.name)
    required_slam = for slam <- slams do
      if slam.filler_name == current_user.name do
        slam
      end
    end
    if required_slam == [nil] or required_slam == [] do
      changeset = %Slam{user_id: user.id} |> Accounts.change_slam
      render(conn, "new.html", changeset: changeset, user: user)
    else
      IO.inspect(slams)
      conn
      |> redirect(to: Routes.user_slam_path(conn, :index, user))
    end
  end

  def create(conn = %{assigns: %{current_user: current_user}}, %{"slam" => slam_params}, user) do
    new_slam_params = slam_params |> Map.put("user_id", user.id) |> Map.put("filler_id", get_session(conn, :user_id)) |> Map.put("filler_name", current_user.name)
    case Accounts.create_slam(new_slam_params) do
      {:ok, slam} ->
        conn
        |> put_flash(:info, "Slam created successfully.")
        |> redirect(to: Routes.user_slam_path(conn, :show, user, slam))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, user: user)
    end
  end

  def show(conn, %{"id" => id}, user) do
    IO.inspect(conn)
    slam = Accounts.get_slam!(user, id)
    render(conn, "show.html", slam: slam, user: user)
  end

  def edit(conn, %{"id" => id}, user) do
    slam = Accounts.get_slam!(user, id)
    changeset = Accounts.change_slam(slam)
    render(conn, "edit.html", slam: slam, changeset: changeset, user: user)
  end

  def update(conn = %{assigns: %{current_user: current_user}}, %{"id" => id, "slam" => slam_params}, user) do
    slam = Accounts.get_slam!(user, id)
    new_slam_params = slam_params |> Map.put("user_id", user.id) |> Map.put("filler_id", get_session(conn, :user_id)) |> Map.put("filler_name", current_user.name)
    case Accounts.update_slam(slam, new_slam_params) do
      {:ok, slam} ->
        conn
        |> put_flash(:info, "Slam updated successfully.")
        |> redirect(to: Routes.user_slam_path(conn, :show, user, slam))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", slam: slam, changeset: changeset, user: user)
    end
  end

  def delete(conn, %{"id" => id}, user) do
    IO.inspect(conn)
    slam = Accounts.get_slam!(user, id)
    {:ok, _slam} = Accounts.delete_slam(slam)
    conn
    |> put_flash(:info, "Slam deleted successfully.")
    |> redirect(to: Routes.user_slam_path(conn, :index, user))
  end

  defp no_self_slam(%{assigns: %{current_user: current_user}, params: %{"user_id" => user_id}}=conn, _params) do
    if(current_user.id == String.to_integer(user_id)) do
      conn
      |> put_flash(:error, "You cannot add a slam to yourself")
      |> redirect(to: Routes.user_path(conn, :index))
    else
      conn
    end
  end

  defp view_my_slam(conn = %{assigns: %{current_user: user}, params: %{"user_id" => user_id}}, _params) do
    if(user.id != String.to_integer(user_id)) do
      conn
      |> put_flash(:error, "You cannot view someone else's slams")
      |> redirect(to: Routes.user_path(conn, :index))
    else
      conn
    end
  end

   defp no_delete(conn = %{assigns: %{current_user: current_user}, params: %{"csrf_token" => token, "_method" => "delete", "id "=> id, "user_id" => user_id}}, _params) do
     slam = Accounts.get_slam!(current_user, String.to_integer(id))
     if slam.filler_name == current_user.name do
       conn
    else
       conn
       |> put_flash(:error, "You cannot delete other contents")
       |> redirect(to: Routes.user_slam_path(conn, :show, current_user, slam))
     end
   end

   defp editing(conn = %{assigns: %{current_user: current_user}, params: %{"id" => id, "user_id" => user_id}}, _) do
     if current_user.id == String.to_integer(user_id) do
       conn
       |> put_flash(:error, "You cannot change other's contents")
       |> redirect(to: Routes.user_slam_path(conn, :index, current_user))
    else
      conn
     end
   end

end

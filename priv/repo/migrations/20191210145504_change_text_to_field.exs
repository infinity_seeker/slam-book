defmodule SlamBook.Repo.Migrations.ChangeTextToField do
  use Ecto.Migration

  def change do
    alter table(:slams) do
      modify :opinion, :text
      modify :suggestion, :text
    end
  end
end

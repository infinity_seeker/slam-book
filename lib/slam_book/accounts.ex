defmodule SlamBook.Accounts do


  import Ecto.Query, warn: false
  alias SlamBook.Repo

  alias SlamBook.Accounts.{User, Credential}


  def list_users do
    Repo.all(User)
  end

  def get_user!(id) do
    Repo.get!(User, id) |> Repo.preload(:credential)
  end


  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.insert()
  end


  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.update()
  end


  def delete_user(%User{} = user) do
    Repo.delete(user)
  end


  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  def authenricate_by_email_password(email, password) do
    cred = Repo.get_by(Credential, email: email) |> Repo.preload(:user)
    cond do
      cred && password == cred.password ->
        {:ok, cred.user}
      cred ->
        {:error, :bad_email_password}
      true ->
        {:ok, :not_found}
    end
  end

  alias SlamBook.Accounts.Credential



  def list_credentials do
    Repo.all(Credential)
  end


  def get_credential!(id), do: Repo.get!(Credential, id)

  def create_credential(attrs \\ %{}) do
    %Credential{}
    |> Credential.changeset(attrs)
    |> Repo.insert()
  end


  def update_credential(%Credential{} = credential, attrs) do
    credential
    |> Credential.changeset(attrs)
    |> Repo.update()
  end


  def delete_credential(%Credential{} = credential) do
    Repo.delete(credential)
  end


  def change_credential(%Credential{} = credential) do
    Credential.changeset(credential, %{})
  end

  alias SlamBook.Accounts.Slam


  def list_slams(user) do
    Slam
    |> where([t], t.user_id == ^user.id)
    |> Repo.all
  end


  def get_slam!(user, id) do
    Slam
    |> where([t], t.user_id == ^ user.id)
    |> Repo.get!(id)
  end


  def create_slam(attrs \\ %{}) do
    %Slam{}
    |> Slam.changeset(attrs)
    |> Repo.insert()
  end


  def update_slam(%Slam{} = slam, attrs) do
    slam
    |> Slam.changeset(attrs)
    |> Repo.update()
  end


  def delete_slam(%Slam{} = slam) do
    Repo.delete(slam)
  end


  def change_slam(%Slam{} = slam) do
    Slam.changeset(slam, %{})
  end
end

defmodule SlamBookWeb.SlamView do
  use SlamBookWeb, :view

  def get_required_slams(slams, user) do
    for slam <- slams do
      if slam.filler_name == user.name do
        slam
      end
    end
  end

end

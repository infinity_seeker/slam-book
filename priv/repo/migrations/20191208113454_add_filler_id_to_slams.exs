defmodule SlamBook.Repo.Migrations.AddFillerIdToSlams do
  use Ecto.Migration

  def change do
    alter table(:slams) do
      add :filler_name, :string
    end
  end
end

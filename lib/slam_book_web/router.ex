defmodule SlamBookWeb.Router do
  use SlamBookWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug SlamBook.Auth, repo: SlamBook.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SlamBookWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/login", SessionController, :new
    get "/logout", SessionController, :delete
    post "/login", SessionController, :create
    resources "/users", UserController do
      resources "/slams", SlamController
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", SlamBookWeb do
  #   pipe_through :api
  # end
end

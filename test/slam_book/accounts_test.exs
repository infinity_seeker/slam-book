defmodule SlamBook.AccountsTest do
  use SlamBook.DataCase

  alias SlamBook.Accounts

  describe "users" do
    alias SlamBook.Accounts.User

    @valid_attrs %{name: "some name", username: "some username"}
    @update_attrs %{name: "some updated name", username: "some updated username"}
    @invalid_attrs %{name: nil, username: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.name == "some name"
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
      assert user.name == "some updated name"
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end

  describe "credentials" do
    alias SlamBook.Accounts.Credential

    @valid_attrs %{email: "some email", password: "some password"}
    @update_attrs %{email: "some updated email", password: "some updated password"}
    @invalid_attrs %{email: nil, password: nil}

    def credential_fixture(attrs \\ %{}) do
      {:ok, credential} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_credential()

      credential
    end

    test "list_credentials/0 returns all credentials" do
      credential = credential_fixture()
      assert Accounts.list_credentials() == [credential]
    end

    test "get_credential!/1 returns the credential with given id" do
      credential = credential_fixture()
      assert Accounts.get_credential!(credential.id) == credential
    end

    test "create_credential/1 with valid data creates a credential" do
      assert {:ok, %Credential{} = credential} = Accounts.create_credential(@valid_attrs)
      assert credential.email == "some email"
      assert credential.password == "some password"
    end

    test "create_credential/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_credential(@invalid_attrs)
    end

    test "update_credential/2 with valid data updates the credential" do
      credential = credential_fixture()
      assert {:ok, %Credential{} = credential} = Accounts.update_credential(credential, @update_attrs)
      assert credential.email == "some updated email"
      assert credential.password == "some updated password"
    end

    test "update_credential/2 with invalid data returns error changeset" do
      credential = credential_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_credential(credential, @invalid_attrs)
      assert credential == Accounts.get_credential!(credential.id)
    end

    test "delete_credential/1 deletes the credential" do
      credential = credential_fixture()
      assert {:ok, %Credential{}} = Accounts.delete_credential(credential)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_credential!(credential.id) end
    end

    test "change_credential/1 returns a credential changeset" do
      credential = credential_fixture()
      assert %Ecto.Changeset{} = Accounts.change_credential(credential)
    end
  end

  describe "slams" do
    alias SlamBook.Accounts.Slam

    @valid_attrs %{advice: "some advice", biggest_negative: "some biggest_negative", biggest_positive: "some biggest_positive", biggest_regret: "some biggest_regret", fancied_me: "some fancied_me", first_impression: "some first_impression", friendship_agremeent: "some friendship_agremeent", had_crush: "some had_crush", last_impression: "some last_impression", opinion: "some opinion", personlality: "some personlality", suggestion: "some suggestion", the_reason: "some the_reason", things_you_hate: "some things_you_hate", things_you_like: "some things_you_like"}
    @update_attrs %{advice: "some updated advice", biggest_negative: "some updated biggest_negative", biggest_positive: "some updated biggest_positive", biggest_regret: "some updated biggest_regret", fancied_me: "some updated fancied_me", first_impression: "some updated first_impression", friendship_agremeent: "some updated friendship_agremeent", had_crush: "some updated had_crush", last_impression: "some updated last_impression", opinion: "some updated opinion", personlality: "some updated personlality", suggestion: "some updated suggestion", the_reason: "some updated the_reason", things_you_hate: "some updated things_you_hate", things_you_like: "some updated things_you_like"}
    @invalid_attrs %{advice: nil, biggest_negative: nil, biggest_positive: nil, biggest_regret: nil, fancied_me: nil, first_impression: nil, friendship_agremeent: nil, had_crush: nil, last_impression: nil, opinion: nil, personlality: nil, suggestion: nil, the_reason: nil, things_you_hate: nil, things_you_like: nil}

    def slam_fixture(attrs \\ %{}) do
      {:ok, slam} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_slam()

      slam
    end

    test "list_slams/0 returns all slams" do
      slam = slam_fixture()
      assert Accounts.list_slams() == [slam]
    end

    test "get_slam!/1 returns the slam with given id" do
      slam = slam_fixture()
      assert Accounts.get_slam!(slam.id) == slam
    end

    test "create_slam/1 with valid data creates a slam" do
      assert {:ok, %Slam{} = slam} = Accounts.create_slam(@valid_attrs)
      assert slam.advice == "some advice"
      assert slam.biggest_negative == "some biggest_negative"
      assert slam.biggest_positive == "some biggest_positive"
      assert slam.biggest_regret == "some biggest_regret"
      assert slam.fancied_me == "some fancied_me"
      assert slam.first_impression == "some first_impression"
      assert slam.friendship_agremeent == "some friendship_agremeent"
      assert slam.had_crush == "some had_crush"
      assert slam.last_impression == "some last_impression"
      assert slam.opinion == "some opinion"
      assert slam.personlality == "some personlality"
      assert slam.suggestion == "some suggestion"
      assert slam.the_reason == "some the_reason"
      assert slam.things_you_hate == "some things_you_hate"
      assert slam.things_you_like == "some things_you_like"
    end

    test "create_slam/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_slam(@invalid_attrs)
    end

    test "update_slam/2 with valid data updates the slam" do
      slam = slam_fixture()
      assert {:ok, %Slam{} = slam} = Accounts.update_slam(slam, @update_attrs)
      assert slam.advice == "some updated advice"
      assert slam.biggest_negative == "some updated biggest_negative"
      assert slam.biggest_positive == "some updated biggest_positive"
      assert slam.biggest_regret == "some updated biggest_regret"
      assert slam.fancied_me == "some updated fancied_me"
      assert slam.first_impression == "some updated first_impression"
      assert slam.friendship_agremeent == "some updated friendship_agremeent"
      assert slam.had_crush == "some updated had_crush"
      assert slam.last_impression == "some updated last_impression"
      assert slam.opinion == "some updated opinion"
      assert slam.personlality == "some updated personlality"
      assert slam.suggestion == "some updated suggestion"
      assert slam.the_reason == "some updated the_reason"
      assert slam.things_you_hate == "some updated things_you_hate"
      assert slam.things_you_like == "some updated things_you_like"
    end

    test "update_slam/2 with invalid data returns error changeset" do
      slam = slam_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_slam(slam, @invalid_attrs)
      assert slam == Accounts.get_slam!(slam.id)
    end

    test "delete_slam/1 deletes the slam" do
      slam = slam_fixture()
      assert {:ok, %Slam{}} = Accounts.delete_slam(slam)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_slam!(slam.id) end
    end

    test "change_slam/1 returns a slam changeset" do
      slam = slam_fixture()
      assert %Ecto.Changeset{} = Accounts.change_slam(slam)
    end
  end
end

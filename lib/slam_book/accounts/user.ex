defmodule SlamBook.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :name, :string
    field :gender, :string
    field :username, :string
    has_one :credential, SlamBook.Accounts.Credential
    has_many :slams, SlamBook.Accounts.Slam
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :username, :gender])
    |> validate_required([:name, :username, :gender])
  end
end

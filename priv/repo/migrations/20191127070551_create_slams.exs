defmodule SlamBook.Repo.Migrations.CreateSlams do
  use Ecto.Migration

  def change do
    create table(:slams) do
      add :first_impression, :text
      add :last_impression, :text
      add :biggest_positive, :text
      add :biggest_negative, :text
      add :fancied_me, :string
      add :the_reason, :text
      add :things_you_like, :text
      add :things_you_hate, :text
      add :biggest_regret, :text
      add :personlality, :string
      add :opinion, :string
      add :had_crush, :string
      add :suggestion, :string
      add :friendship_agremeent, :text
      add :advice, :text
      add :user_id, references(:users, on_delete: :delete_all)
      add :filler_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:slams, [:user_id])
    create index(:slams, [:filler_id])
  end
end

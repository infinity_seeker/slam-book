defmodule SlamBookWeb.SlamControllerTest do
  use SlamBookWeb.ConnCase

  alias SlamBook.Accounts

  @create_attrs %{advice: "some advice", biggest_negative: "some biggest_negative", biggest_positive: "some biggest_positive", biggest_regret: "some biggest_regret", fancied_me: "some fancied_me", first_impression: "some first_impression", friendship_agremeent: "some friendship_agremeent", had_crush: "some had_crush", last_impression: "some last_impression", opinion: "some opinion", personlality: "some personlality", suggestion: "some suggestion", the_reason: "some the_reason", things_you_hate: "some things_you_hate", things_you_like: "some things_you_like"}
  @update_attrs %{advice: "some updated advice", biggest_negative: "some updated biggest_negative", biggest_positive: "some updated biggest_positive", biggest_regret: "some updated biggest_regret", fancied_me: "some updated fancied_me", first_impression: "some updated first_impression", friendship_agremeent: "some updated friendship_agremeent", had_crush: "some updated had_crush", last_impression: "some updated last_impression", opinion: "some updated opinion", personlality: "some updated personlality", suggestion: "some updated suggestion", the_reason: "some updated the_reason", things_you_hate: "some updated things_you_hate", things_you_like: "some updated things_you_like"}
  @invalid_attrs %{advice: nil, biggest_negative: nil, biggest_positive: nil, biggest_regret: nil, fancied_me: nil, first_impression: nil, friendship_agremeent: nil, had_crush: nil, last_impression: nil, opinion: nil, personlality: nil, suggestion: nil, the_reason: nil, things_you_hate: nil, things_you_like: nil}

  def fixture(:slam) do
    {:ok, slam} = Accounts.create_slam(@create_attrs)
    slam
  end

  describe "index" do
    test "lists all slams", %{conn: conn} do
      conn = get(conn, Routes.slam_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Slams"
    end
  end

  describe "new slam" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.slam_path(conn, :new))
      assert html_response(conn, 200) =~ "New Slam"
    end
  end

  describe "create slam" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.slam_path(conn, :create), slam: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.slam_path(conn, :show, id)

      conn = get(conn, Routes.slam_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Slam"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.slam_path(conn, :create), slam: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Slam"
    end
  end

  describe "edit slam" do
    setup [:create_slam]

    test "renders form for editing chosen slam", %{conn: conn, slam: slam} do
      conn = get(conn, Routes.slam_path(conn, :edit, slam))
      assert html_response(conn, 200) =~ "Edit Slam"
    end
  end

  describe "update slam" do
    setup [:create_slam]

    test "redirects when data is valid", %{conn: conn, slam: slam} do
      conn = put(conn, Routes.slam_path(conn, :update, slam), slam: @update_attrs)
      assert redirected_to(conn) == Routes.slam_path(conn, :show, slam)

      conn = get(conn, Routes.slam_path(conn, :show, slam))
      assert html_response(conn, 200) =~ "some updated advice"
    end

    test "renders errors when data is invalid", %{conn: conn, slam: slam} do
      conn = put(conn, Routes.slam_path(conn, :update, slam), slam: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Slam"
    end
  end

  describe "delete slam" do
    setup [:create_slam]

    test "deletes chosen slam", %{conn: conn, slam: slam} do
      conn = delete(conn, Routes.slam_path(conn, :delete, slam))
      assert redirected_to(conn) == Routes.slam_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.slam_path(conn, :show, slam))
      end
    end
  end

  defp create_slam(_) do
    slam = fixture(:slam)
    {:ok, slam: slam}
  end
end

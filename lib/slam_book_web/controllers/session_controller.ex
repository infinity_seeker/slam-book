defmodule SlamBookWeb.SessionController do
  use SlamBookWeb, :controller

  alias SlamBook.Accounts

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"user" => %{"email" => "", "password" => ""}}) do
    conn
    |> put_flash(:error, "Please enter the details")
    |> redirect(to: Routes.session_path(conn, :new))
  end

  def create(conn, %{"user" => %{"email" => email, "password" => password}}) do
    if email == "" or password == "" do
      conn
      |> put_flash(:error, "Enter valid details")
      |> redirect(to: Routes.session_path(conn, :new))
    else
      case Accounts.authenricate_by_email_password(email, password) do
        {:ok, user} ->
          conn
          |> put_session(:user_id, user.id)
          |> put_flash(:info, "Welcome #{user.name}")
          |> configure_session(renew: true)
          |> redirect(to: Routes.user_path(conn, :index))
        {:error, :bad_email_password} ->
          conn
          |> put_flash(:error, "Bad email/password")
          |> redirect(to: Routes.session_path(conn, :new))
        {:error, :not_found} ->
          conn
          |> put_flash(:error, "No account found")
          |> redirect(to: Routes.user_path(conn, :new))
      end
    end
  end

  def delete(conn, _) do
    conn
    |> configure_session(drop: true)
    |> redirect(to: "/")
  end

end
